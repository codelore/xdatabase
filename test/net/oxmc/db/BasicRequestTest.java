package net.oxmc.db;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.*;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Тестирует простые INSERT/SELECT запросы в одной таблице
 */
public class BasicRequestTest {
    private static Logger logger = Logger.getLogger("Test");
    private static DBStatement createTable, insert, getByName;

    @BeforeClass
    public static void setup() throws SQLException {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:testdb");
        Database.setDataSource(dataSource);
        Database.setLogger(logger);

        createTable = new DBStatement("CREATE TABLE test (id INT AUTO_INCREMENT, name CHAR(255), data BLOB, PRIMARY KEY(id))");
        insert = new DBStatement("INSERT INTO test(name, data) VALUES(?, ?)");
        getByName = new DBStatement("SELECT id,data FROM test WHERE name=?");

        createTable.executeUpdate();

        Assert.assertEquals(1, insert.executeInsert("Test1", null));
        Assert.assertEquals(2, insert.executeInsert("Test2", null));
        Assert.assertEquals(3, insert.executeInsert("Test3", null));
        Assert.assertNotNull(insert.executeInsert("Test4", null));
        Assert.assertNotNull(insert.executeInsert("Test5", null));
        Assert.assertNotNull(insert.executeInsert("Test6", createBlob(new byte[]{1, 2, 5, 6})));
        Assert.assertNotNull(insert.executeInsert("Test7", null));
        Assert.assertNotNull(insert.executeInsert("Test7", null));
    }

    @Test
    public void selectNothing() throws SQLException {
        Object results[][] = getByName.executeQuery("Test");
        Assert.assertArrayEquals(new Object[0][0], results);
    }

    @Test
    public void selectUnique() throws SQLException {
        Object results[] = getByName.executeOneResultQuery("Test1");
        Assert.assertEquals(null, results[1]);

        results = getByName.executeOneResultQuery("Test6");
        Assert.assertArrayEquals(new byte[]{1, 2, 5, 6}, (byte[]) results[1]);
    }

    @Test
    public void selectTwo() throws SQLException {
        Object results[][] = getByName.executeQuery("Test7");
        Assert.assertArrayEquals(new Object[][] {{7, null}, {8, null}}, results);
    }

    @AfterClass
    public static void shutdown() {
        Database.shutdown();
    }

    private static Blob createBlob(byte[] bytes) throws SQLException {
        return new SerialBlob(bytes);
    }
}
