package net.oxmc.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBStatement {
    private static Logger logger = XDatabase.logger;

    private String sql;

    public DBStatement(String sql) {
        this.sql = sql;
    }

    /**
     * Выполняет запрос на обновление данных в БД
     * @param params параметры запроса
     * @return количество измененных строк
     */
    public int executeUpdate(Object... params) throws SQLException {
        Connection connection = Database.getConnection();
        if (connection == null) {
            throw new SQLException("No connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            return statement.executeUpdate();
        } finally {
            connection.close();
        }
    }

    /**
     * Выполняет запрос на вставление строки в БД
     * Возвращает PRIMARY_KEY новой записи, null если запись не была добавлена или PRIMARY_KEY не был получен
     */
    public Object executeInsert(Object... params) throws SQLException {
        Connection connection = Database.getConnection();
        PreparedStatement statement = null;
        if (connection == null) {
            throw new SQLException("No connection");
        }
        try {
            statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs != null && rs.first()) {
                return rs.getInt(1);
            }
        } finally {
            closeQuietly(statement);
            connection.close();
        }
        return null;
    }

    /**
     * Выполняет запрос на получение данных из БД
     * @param params параметры запроса
     * @return массив данных, находящихся в столбцах, null, если нет данных
     *
     * Примечание: BLOB возвращается как byte[]
     */
    public Object[] executeOneResultQuery(Object... params) throws SQLException {
        Object[][] results = executeQuery(params);
        if (results.length > 0) {
            return results[0];
        } else {
            return null;
        }
    }

    /**
     * Выполняет запрос на получение данных в БД
     * @param params параметры запроса
     * @return массив: array[i][j] - содержимое j столбца, i строки ответа БД
     *
     * Примечание: BLOB возвращается как byte[]
     *
     */
    public Object[][] executeQuery(Object... params) throws SQLException {
        Connection connection = Database.getConnection();
        if (connection == null) {
            throw new SQLException("No connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<Object[]> rowsList = new ArrayList<Object[]>();
            int columnCount = resultSet.getMetaData().getColumnCount();

            while (resultSet.next()) {
                Object[] data = new Object[columnCount];
                for (int i = 0; i < data.length; i++) {
                    data[i] = resultSet.getObject(i + 1);
                    if (data[i] instanceof Blob) {
                        Blob blob = (Blob) data[i];
                        data[i] = blob.getBytes(0, (int) blob.length());
                        blob.free();
                    }
                }
                rowsList.add(data);
            }
            Object[][] rows = new Object[rowsList.size()][];
            for (int i = 0; i < rowsList.size(); i++) {
                rows[i] = rowsList.get(i);
            }
            return rows;
        } finally {
            connection.close();
        }
    }

    private void closeQuietly(PreparedStatement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Error closing prepared statement", e);
            }
        }
    }
}

